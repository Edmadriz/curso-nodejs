import express from "express"
import config from "./config"
import sequelize from './database/connection';

import productsRoutes from "./routes/productsRoutes"
import usersRoutes from "./routes/users"
import auth from "./routes/auth"

const app = express()

// settings
app.set('port', config.port)


// middleware
app.use(express.json());
app.use(express.urlencoded({extended: false}));


// routes
app.use("/v1", productsRoutes)
app.use("/v1", usersRoutes)
app.use("/v1", auth)



app.listen(app.get('port'), () => {
    console.log(`Running server on http://localhost:${app.get('port')}`);

    sequelize.sync({force: false}).then(() => {
        console.log("Conexion a la Base de Datos Exitosa..!")
    }).catch(error => {
        console.log('Se ha Producido un Error: ', error);
    })
});

