import { create, getAll, getOneById, deleteOne, updateOne, countAll } from "../services/users";
import { createToken } from "../helpers/createToken";


export const countAllUsers = async (req, res) => {
    try {
        const result = await countAll();

        res.status(200).json({"object": result, "code": "200", "message": "OK"});
    
    } catch (error) {
        console.log(error);
        res.status(500).send({"code": "500", "message": "SERVER ERROR"});
    }
};

export const getAllUsers = async (req, res) => {
    try {
        const result = await getAll();
        res.status(200).json({"object": result, "code": "200", "message": "OK"});
    } catch (error) {
        res.status(500).send({"code": "500", "message": "SERVER ERROR"});
    }
};

export const getUserById = async (req, res) => {
    const { id } = req.params;

    try {
        const result = await getOneById(id);

        if (!result){
            return res.status(404).send({"code": "404", "message": "REQUEST NOT FOUND"});
        }

        res.status(200).json({"object": result, "code": "200", "message": "OK"});
    
    } catch (error) {
        res.status(500).send({"code": "500", "message": "SERVER ERROR"});
    }
};

export const createOneUser = async (req, res) => {
    const { username, password} = req.body;

    try {
        const result = await create(username, password);
        res.status(201).json({"object": result, "code": "201", "message": "OK", "token": createToken(result)});
    } catch (error) {
        res.status(500).send({"code": "500", "message": "SERVER ERROR"});
    }
};

export const updateUserById = async (req, res) => {
    const { id } = req.params;

    try {
        const user = await getOneById(id);

        if (!user){
            return res.status(404).send({"code": "404", "message": "REQUEST NOT FOUND"});
        }

        const result = await updateOne(user, req.body);

        res.status(200).json({"object": result, "code": "200", "message": "OK"});
        
    } catch (error) {
        res.status(500).send({"code": "500", "message": "SERVER ERROR"});
    }
};

export const deleteOneUser = async (req, res) => {
    const { id } = req.params;

    try {
        const user = await getOneById(id);

        if (!user){
            return res.status(404).send({"code": "404", "message": "REQUEST NOT FOUND"});
        }

        await deleteOne(id);
        res.status(200).json({"object": user, "code": "200", "message": "OK"});
        
    } catch (error) {
        res.status(500).send({"code": "500", "message": "SERVER ERROR"});
    }
};