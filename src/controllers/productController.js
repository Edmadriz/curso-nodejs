import { create, getAll, getOneById, deleteOne, updateOne, countAll } from "../services/productService";


export const countAllProducts = async (req, res) => {
    try {
        const result = await countAll();

        res.status(200).json({"object": result, "code": "200", "message": "OK"});
    
    } catch (error) {
        console.log(error);
        res.status(500).send({"code": "500", "message": "SERVER ERROR"});
    }
};

export const getAllProducts = async (req, res) => {
    try {
        const products = await getAll();
        res.status(200).json({"object": products, "code": "200", "message": "OK"});
    } catch (error) {
        res.status(500).send({"code": "500", "message": "SERVER ERROR"});
    }
};

export const getProducById = async (req, res) => {
    const { id } = req.params;

    try {
        const product = await getOneById(id);

        if (!product){
            return res.status(404).send({"code": "404", "message": "REQUEST NOT FOUND"});
        }

        res.status(200).json({"object": product, "code": "200", "message": "OK"});
    
    } catch (error) {
        res.status(500).send({"code": "500", "message": "SERVER ERROR"});
    }
};

export const createOneProduct = async (req, res) => {
    const { name, description, quantity} = req.body;

    try {
        const newProduct = await create(name, description, quantity);
        res.status(201).json({"object": newProduct, "code": "201", "message": "OK"});
    } catch (error) {
        res.status(500).send({"code": "500", "message": "SERVER ERROR"});
    }
};

export const updateProducById = async (req, res) => {
    const { id } = req.params;

    try {
        const product = await getOneById(id);

        if (!product){
            return res.status(404).send({"code": "404", "message": "REQUEST NOT FOUND"});
        }

        const productUpdated = await updateOne(product, req.body);

        res.status(200).json({"object": productUpdated, "code": "200", "message": "OK"});
        
    } catch (error) {
        res.status(500).send({"code": "500", "message": "SERVER ERROR"});
    }
};

export const deleteOneProduct = async (req, res) => {
    const { id } = req.params;

    try {
        const product = await getOneById(id);

        if (!product){
            return res.status(404).send({"code": "404", "message": "REQUEST NOT FOUND"});
        }

        await deleteOne(id);
        res.status(200).json({"object": product, "code": "200", "message": "OK"});
        
    } catch (error) {
        res.status(500).send({"code": "500", "message": "SERVER ERROR"});
    }
};