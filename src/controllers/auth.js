import { getOneByUsername } from "../services/auth";
import {createToken} from "../helpers/createToken";

export const singnIn = async (req, res) => {
    const { username, password} = req.body;

    try {
        const user = await getOneByUsername(username);

        if (!user){
            return res.status(404).send({"code": "404", "message": "REQUEST NOT FOUND"});
        }

        if (user.password != password){
            return res.status(400).send({"code": "400", "message": "BAD REQUEST", "errors": "Contrasenia incorrecta"});
        }

        req.user = user.id;

        res.status(200).json({"object": user, "code": "200", "message": "OK", "token": createToken(user)});
    
    } catch (error) {
        console.log(error);
        res.status(500).send({"code": "500", "message": "SERVER ERROR"});
    }
};

