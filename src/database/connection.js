import config from '../config'
import { Sequelize } from 'sequelize';

const sequelize = new Sequelize(config.dbDatabase, config.dbUser, config.dbPassword, {
    host: config.dbServer,
    dialect: 'mssql',
});

export default sequelize;