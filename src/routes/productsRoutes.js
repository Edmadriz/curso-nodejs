import { Router } from "express";
import {validateProduct} from "../middleware/validators/products"
import { createOneProduct, getAllProducts, getProducById, deleteOneProduct, countAllProducts, updateProducById} from "../controllers/productController";

const router = Router();

router.get('/products/count', countAllProducts);

router.get('/products', getAllProducts);

router.get('/products/:id', getProducById);

router.post('/products', validateProduct, createOneProduct);

router.put('/products/:id', validateProduct, updateProducById);

router.delete('/products/:id', deleteOneProduct);


export default router