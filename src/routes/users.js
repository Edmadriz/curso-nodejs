import { Router } from "express";
import { validateUser } from "../middleware/validators/users"
import { createOneUser, getAllUsers, getUserById, deleteOneUser, countAllUsers, updateUserById } from "../controllers/users";
import { isAuth } from "../middleware/auth";


const router = Router();

router.get('/users/count', countAllUsers);

router.get('/users', getAllUsers);

router.get('/users/:id', getUserById);

router.post('/users', validateUser, createOneUser);

router.put('/users/:id', validateUser, updateUserById);

router.delete('/users/:id', isAuth, deleteOneUser);


export default router