import jwt from 'jwt-simple'
import config from '../config'

export const createToken = (user) => {
    const payload = {
        id: user.id,
        username: user.username
    }

    return jwt.encode(payload, config.SECRET_TOKEN);
}