import {validationResult} from 'express-validator'


export const validateResult = (req, res, next) => {
    try{
        validationResult(req).throw()
        return next();
    }catch (error) {
        res.status(400);
        res.send(
            {
                "code": "400", 
                "message": "BAD REQUEST",
                "errors": error.array()
            }
        );
    }
}
