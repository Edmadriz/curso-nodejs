import { check } from "express-validator";
import {validateResult} from "../../helpers/validateHelper"


export const validateProduct = [
    check('name')
    .exists()
    .not()
    .isEmpty(),
    check('description')
    .exists()
    .not()
    .isEmpty(),
    check('quantity')
    .exists()
    .isNumeric()
    .isInt()
    .not()
    .isEmpty(),
    (req, res, next) => {
        validateResult(req, res, next);
    }
]
