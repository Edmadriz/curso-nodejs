import { check } from "express-validator";
import {validateResult} from "../../helpers/validateHelper"


export const validateUser = [
    check('username')
    .exists()
    .not()
    .isEmpty(),
    check('password')
    .exists()
    .not()
    .isEmpty(),
    (req, res, next) => {
        validateResult(req, res, next);
    }
]