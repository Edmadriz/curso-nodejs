import jwt from 'jwt-simple'
import config from '../config'

export const isAuth = (req, res, next) => {
    console.log(req.headers);
    if (!req.headers.authorization) {
        return res.status(403).send({"code": "403", "message": "Unauthorized"});
    }

    const token = req.headers.authorization.split(" ")[1];
    console.log(token);
    const payload = jwt.decode(token, config.SECRET_TOKEN);

    req.user = payload.id;
    next();
}