import { Model, DataTypes  } from 'sequelize'
import sequelize from '../database/connection';

class Product extends Model {} 
Product.init({
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    quantity: DataTypes.INTEGER
}, {
    sequelize,
    modelName: "products"
});

export default Product;