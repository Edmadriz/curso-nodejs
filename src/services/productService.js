import Product from "../models/Product";


export const countAll = async () => {
    const result = await Product.count();
    return result;
}

export const getAll = async () => {
    const products = await Product.findAll();
    return products;
}

export const getOneById = async (id) => {
    const product = await Product.findByPk(id);
    return product;
}

export const create = async (name, description, quantity) => {
    const newProduct = await Product.create({name, description, quantity});
    return newProduct;
}


export const updateOne = async (product, body) => {
    const { name, description, quantity} = body;

    product.name = name;
    product.description = description;
    product.quantity = quantity;
    await product.save();

    return product;
}

export const deleteOne = async (id) => {
    await Product.destroy({
        where: {
            id,
        }
    });
}