import User from "../models/User";


export const countAll = async () => {
    const result = await User.count();
    return result;
}

export const getAll = async () => {
    const result = await User.findAll();
    return result;
}

export const getOneById = async (id) => {
    const result = await User.findByPk(id);
    return result;
}

export const create = async (username, password) => {
    const result = await User.create({username, password});
    return result;
}


export const updateOne = async (user, body) => {
    const { username, password} = body;

    user.username = username;
    user.password = password;
    await user.save();
    return user;
}

export const deleteOne = async (id) => {
    await User.destroy({
        where: {
            id,
        }
    });
}