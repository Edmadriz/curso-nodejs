import User from "../models/User";

export const getOneByUsername = async (username) => {
    const result = await User.findOne(
        { 
            where: { 
                username 
            } 
        }
    );
    return result;
}

